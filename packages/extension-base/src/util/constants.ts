// Copyright 2019-2024 @polkadot/extension-base authors & contributors
// SPDX-License-Identifier: Apache-2.0

// Duniter network endpoint
export const DUNITER_NETWORK_ENDPOINT = 'wss://gdev.p2p.legal/ws';

// Local storage keys
export const LOCAL_STORAGE_SCAN_DERIVATION_KEY = 'scanDerivationEnabled';
export const LOCAL_STORAGE_NO_PASSWORD = 'noPasswordPreference';