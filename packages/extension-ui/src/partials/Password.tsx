// Copyright 2019-2025 @polkadot/extension-ui authors & contributors
// SPDX-License-Identifier: Apache-2.0

import type { Validator } from '../util/validators.js';

import React, { useCallback, useEffect, useMemo, useState } from 'react';

import { Checkbox, InputWithLabel, ValidatedInput } from '../components/index.js';
import { useTranslation } from '../hooks/index.js';
import { allOf, isNotShorterThan, isSameAs } from '../util/validators.js';
import { LOCAL_STORAGE_NO_PASSWORD } from '@polkadot/extension-base/util/constants';

interface Props {
  isFocussed?: boolean;
  onChange: (password: string | null, isNoPassword: boolean) => void;
}

const MIN_LENGTH = 6;

export default function Password ({ isFocussed, onChange }: Props): React.ReactElement<Props> {
  const { t } = useTranslation();
  const [pass1, setPass1] = useState<string | null>(null);
  const [pass2, setPass2] = useState<string | null>(null);
  const [noPassword, setNoPassword] = useState(() => {
    // Retrieve the initial state from localStorage
    const storedValue = localStorage.getItem(LOCAL_STORAGE_NO_PASSWORD);
    return storedValue ? JSON.parse(storedValue) : false;
  });

  const isFirstPasswordValid = useMemo(() => isNotShorterThan(MIN_LENGTH, t('Password is too short')), [t]);
  const isSecondPasswordValid = useCallback((firstPassword: string): Validator<string> => allOf(
    isNotShorterThan(MIN_LENGTH, t('Password is too short')),
    isSameAs(firstPassword, t('Passwords do not match'))
  ), [t]);

  useEffect((): void => {
    if (noPassword) {
      onChange(null, true);
    } else if (pass1 && pass2 && pass1 === pass2) {
      onChange(pass1, false);
    } else {
      onChange(null, false);
    }
  }, [onChange, pass1, pass2, noPassword]);

  const handleNoPasswordChange = useCallback((checked: boolean) => {
    setNoPassword(checked);
    // Save the state in localStorage
    localStorage.setItem(LOCAL_STORAGE_NO_PASSWORD, JSON.stringify(checked));
    if (checked) {
      setPass1(null);
      setPass2(null);
    }
  }, []);

  return (
    <>
      {!noPassword && (
        <>
          <ValidatedInput
            component={InputWithLabel}
            data-input-password
            isFocused={isFocussed}
            label={t('A new password for this account')}
            onValidatedChange={setPass1}
            type='password'
            validator={isFirstPasswordValid}
          />
          {pass1 && (
            <ValidatedInput
              component={InputWithLabel}
              data-input-repeat-password
              label={t('Repeat password for verification')}
              onValidatedChange={setPass2}
              type='password'
              validator={isSecondPasswordValid(pass1)}
            />
          )}
        </>
      )}
      <Checkbox
        checked={noPassword}
        label={t("No password, I'm a boss 😎")}
        onChange={handleNoPasswordChange}
      />
    </>
  );
}
