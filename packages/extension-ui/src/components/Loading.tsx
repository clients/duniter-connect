// Copyright 2019-2025 @polkadot/extension-ui authors & contributors
// SPDX-License-Identifier: Apache-2.0

import React from 'react';
import styled, { keyframes } from 'styled-components';
import { useTranslation } from '../hooks/index.js';
import duniterAnimated from '../assets/duniter-animated.svg';

interface Props {
  children?: React.ReactNode;
}

const LoadingWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 100vh;
  overflow: hidden;
  position: relative;
`;

const AnimatedSVG = styled.img`
  width: 300px;
  height: 300px;
`;

const dotsKeyframes = keyframes`
  0% { content: ""; }
  33% { content: "."; }
  66% { content: ".."; }
  100% { content: "..."; }
`;

const Message = styled.div`
  margin-top: 20px;
  text-align: center;
  font-size: 18px;

  &::after {
    content: "";
    display: inline-block;
    width: 1.2em;
    text-align: left;
    animation: ${dotsKeyframes} 1.5s infinite steps(1, end);
  }
`;

export default function Loading({ children }: Props): React.ReactElement<Props> {
  const { t } = useTranslation();

  if (children) {
    return <>{children}</>;
  }

  return (
    <LoadingWrapper>
      <AnimatedSVG src={duniterAnimated} alt="Loading animation" />
      <Message>{t('Loading in progress')}</Message>
    </LoadingWrapper>
  );
}
