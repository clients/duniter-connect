// Copyright 2017-2025 @polkadot/extension-ui authors & contributors
// SPDX-License-Identifier: Apache-2.0

import type { DropzoneRef } from 'react-dropzone';

import React, { createRef, useCallback, useState } from 'react';
import Dropzone from 'react-dropzone';

import { formatNumber, hexToU8a, isHex, u8aToString } from '@polkadot/util';

import { useTranslation } from '../hooks/index.js';
import { styled } from '../styled.js';
import Label from './Label.js';

function classes (...classNames: (boolean | null | string | undefined)[]): string {
  return classNames
    .filter((className): boolean => !!className)
    .join(' ');
}

export interface InputFileProps {
  className?: string;
  accept?: string;
  clearContent?: boolean;
  convertHex?: boolean;
  help?: React.ReactNode;
  isDisabled?: boolean;
  isError?: boolean;
  label: string;
  onChange?: (contents: Uint8Array, name: string, type: 'hex' | 'pubsec' | 'json' | 'other') => void;
  placeholder?: React.ReactNode | null;
  withEllipsis?: boolean;
  withLabel?: boolean;
}

interface FileState {
  name: string;
  size: number;
  type: 'hex' | 'pubsec' | 'json' | 'other';
}

const BYTE_STR_0 = '0'.charCodeAt(0);
const BYTE_STR_X = 'x'.charCodeAt(0);
const NOOP = (): void => undefined;

function convertResult (result: ArrayBuffer, convertHex?: boolean): { data: Uint8Array, type: 'hex' | 'pubsec' | 'json' | 'other' } {
  const data = new Uint8Array(result);
  const textContent = u8aToString(data);

  if (textContent.startsWith('Type: PubSec')) {
    return { data, type: 'pubsec' };
  }

  if (convertHex && data[0] === BYTE_STR_0 && data[1] === BYTE_STR_X) {
    const hex = u8aToString(data);

    if (isHex(hex)) {
      return { data: hexToU8a(hex), type: 'hex' };
    }
  }

  try {
    JSON.parse(textContent);
    return { data, type: 'json' };
  } catch {
    // If it's not JSON, we consider it as 'other'
  }

  return { data, type: 'other' };
}

function InputFile ({ className = '', clearContent, convertHex, isDisabled, isError = false, label, onChange, placeholder }: InputFileProps): React.ReactElement<InputFileProps> {
  const { t } = useTranslation();
  const dropRef = createRef<DropzoneRef>();
  const [file, setFile] = useState<FileState | undefined>();

  const _onDrop = useCallback(
    (files: File[]): void => {
      files.forEach((file): void => {
          const reader = new FileReader();

        reader.onabort = NOOP;
        reader.onerror = NOOP;

        reader.onload = ({ target }: ProgressEvent<FileReader>): void => {
          if (target?.result) {
            const name = file.name;
            const { data, type } = convertResult(target.result as ArrayBuffer, convertHex);

            onChange && onChange(data, name, type);
            dropRef && setFile({
              name,
              size: data.length,
              type
            });
          }
        };

        reader.readAsArrayBuffer(file);
      });
    },
    [convertHex, dropRef, onChange]
  );

  const dropZone = (
    <Dropzone
      disabled={isDisabled}
      multiple={false}
      onDrop={_onDrop}
      ref={dropRef}
    >
      {({ getInputProps, getRootProps }): React.ReactElement => (
        <div {...getRootProps({ className: classes('ui--InputFile', isError ? 'error' : '', className) })}>
          <input {...getInputProps()} />
          <em className='label'>
            {
              !file || clearContent
                ? placeholder || t('click to select or drag and drop the file here')
                : placeholder || t('{{name}} ({{size}} bytes, {{type}})', {
                    replace: {
                      name: file.name,
                      size: formatNumber(file.size),
                      type: file.type
                    }
                  })
            }
          </em>
        </div>
      )}
    </Dropzone>
  );

  return label
    ? (
      <Label
        label={label}
      >
        {dropZone}
      </Label>
    )
    : dropZone;
}

export default React.memo(styled(InputFile)<InputFileProps>(({ isError }) => `
  border: 1px solid var(${isError ? '--errorBorderColor' : '--inputBorderColor'});
  background: var(--inputBackground);
  border-radius: var(--borderRadius);
  color: var(${isError ? '--errorBorderColor' : '--textColor'});
  font-size: 1rem;
  margin: 0.25rem 0;
  overflow-wrap: anywhere;
  padding: 0.5rem 0.75rem;

  &:hover {
    cursor: pointer;
  }
`));
