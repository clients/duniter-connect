// Copyright 2019-2025 @polkadot/extension-ui authors & contributors
// SPDX-License-Identifier: Apache-2.0

import React, { useCallback, useState } from 'react';

import { Name, Password } from '../partials/index.js';
import { BackButton, ButtonArea, NextStepButton, VerticalSpace } from './index.js';

interface Props {
  buttonLabel?: string;
  isBusy: boolean;
  onBackClick?: () => void;
  onCreate: (name: string, password: string | null) => void | Promise<void | boolean>;
  onNameChange: (name: string) => void;
  onPasswordChange?: (password: string | null, isNoPassword: boolean) => void;
}

function AccountNamePasswordCreation ({ buttonLabel, isBusy, onBackClick, onCreate, onNameChange, onPasswordChange }: Props): React.ReactElement<Props> {
  const [name, setName] = useState<string | null>(null);
  const [password, setPassword] = useState<string | null>(null);
  const [noPassword, setNoPassword] = useState(false);

  const _onCreate = useCallback(
    (): void => {
      if (name && (password || noPassword)) {
        console.log('Creating account with:', { name, password: noPassword ? null : password });
        Promise
          .resolve(onCreate(name, noPassword ? null : password))
          .catch((error) => {
            console.error('Error creating account:', error);
          });
      } else {
        console.error('Cannot create account: Name is missing or password is required');
      }
    },
    [name, password, noPassword, onCreate]
  );

  const _onNameChange = useCallback(
    (name: string | null) => {
      onNameChange(name || '');
      setName(name);
    },
    [onNameChange]
  );

  const _onPasswordChange = useCallback((newPassword: string | null, isNoPassword: boolean) => {
    setPassword(newPassword || '');
    setNoPassword(isNoPassword);
    onPasswordChange && onPasswordChange(newPassword, isNoPassword);
  }, [onPasswordChange]);
  

  const _onBackClick = useCallback(
    () => {
      _onNameChange(null);
      setPassword(null);
      setNoPassword(false);
      onBackClick && onBackClick();
    },
    [_onNameChange, onBackClick]
  );

  const isButtonDisabled = !name || (!noPassword && !password);

  return (
    <>
      <Name
        isFocused
        onChange={_onNameChange}
      />
      <Password 
        onChange={_onPasswordChange}
      />
      <VerticalSpace />
      {onBackClick && buttonLabel && (
        <ButtonArea>
          <BackButton onClick={_onBackClick} />
          <NextStepButton
            data-button-action='add new root'
            isBusy={isBusy}
            isDisabled={isButtonDisabled}
            onClick={_onCreate}
          >
            {buttonLabel}
          </NextStepButton>
        </ButtonArea>
      )}
    </>
  );
}

export default React.memo(AccountNamePasswordCreation);
