// Copyright 2019-2024 @polkadot/extension-ui authors & contributors
// SPDX-License-Identifier: Apache-2.0

import React, { useCallback, useState, useEffect, useRef } from 'react';
import { useTranslation } from '../hooks/index.js';
import { Input, Switch } from '../components/index.js';
import { convertPubKeyToSS58, convertSS58ToPubKey } from '../messaging.js';
import styled from 'styled-components';
import { Header } from '../partials/index.js';

const Container = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 20px 10px;
  width: 90%;
  margin: 0 auto;
`;

const StyledSwitch = styled(Switch)`
  margin-bottom: 20px;
`;

const InputContainer = styled.div`
  width: 100%;
  margin-bottom: 20px;
`;

function ConvertPubkeyAddress(): React.ReactElement {
  const { t } = useTranslation();
  const [input, setInput] = useState<string>('');
  const [output, setOutput] = useState<string>('');
  const [ss58Prefix, setSS58Prefix] = useState<string>('42');
  const [isReverse, setIsReverse] = useState<boolean>(false);
  const inputRef = useRef<HTMLInputElement>(null);

  const handleInputChange = useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
    setInput(event.target.value);
  }, []);

  const handleSS58PrefixChange = useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
    setSS58Prefix(event.target.value);
  }, []);

  const toggleMode = useCallback(() => {
    setIsReverse(!isReverse);
    setInput('');
    setOutput('');
  }, [isReverse]);

  useEffect(() => {
    const convert = async () => {
      if (input) {
        try {
          const result = isReverse
            ? await convertSS58ToPubKey(input)
            : await convertPubKeyToSS58(input, parseInt(ss58Prefix));
          setOutput(result);
        } catch (error) {
          console.error('Erreur de conversion:', error);
          setOutput('');
        }
      } else {
        setOutput('');
      }
    };

    convert();
  }, [isReverse, input, ss58Prefix]);

  useEffect(() => {
      inputRef.current?.focus();
  });

  return (
    <>
      <Header
        showBackArrow
        text={t('Convertisseur Pubkey / Address')}
      />
      <Container>
        <StyledSwitch
          checked={isReverse}
          onChange={toggleMode}
          checkedLabel={t('Address vers Pubkey')}
          uncheckedLabel={t('Pubkey vers Address')}
        />
        <InputContainer>
          {!isReverse && (
            <Input
              placeholder={t('Préfixe SS58')}
              onChange={handleSS58PrefixChange}
              value={ss58Prefix}
              type="number"
            />
          )}
        </InputContainer>
        <InputContainer>
          <Input
            placeholder={isReverse ? t('Adresse SS58') : t('Clé publique Base58')}
            onChange={handleInputChange}
            value={input}
            ref={inputRef}
            autoFocus
          />
        </InputContainer>
        <InputContainer>
          <Input
            placeholder={isReverse ? t('Clé publique Base58') : t('Adresse SS58')}
            value={output}
            readOnly
          />
        </InputContainer>
      </Container>
    </>
  );
}

export default ConvertPubkeyAddress;
