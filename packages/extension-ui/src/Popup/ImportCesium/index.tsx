// Copyright 2019-2023 @polkadot/extension-ui authors & contributors
// SPDX-License-Identifier: Apache-2.0

import type { HexString } from '@polkadot/util/types';

import React, { useCallback, useContext, useEffect, useState } from 'react';

import AccountNamePasswordCreation from '../../components/AccountNamePasswordCreation.js';
import { AccountContext, ActionContext, Address } from '../../components/index.js';
import { useTranslation } from '../../hooks/index.js';
import { createAccountCesium } from '../../messaging.js';
import { HeaderWithSteps } from '../../partials/index.js';
import CesiumIdPwd from './CesiumIdPwd.js';

export interface AccountInfo {
  address: string;
  genesis?: HexString;
  seedHex: string;
}

function ImportCesium (): React.ReactElement {
  const { t } = useTranslation();
  const { accounts } = useContext(AccountContext);
  const onAction = useContext(ActionContext);
  const [isBusy, setIsBusy] = useState(false);
  const [account, setAccount] = useState<AccountInfo | null>(null);
  const [name, setName] = useState<string | null>(null);
  const [password, setPassword] = useState<string>('');
  const [noPassword, setNoPassword] = useState(false);
  const [step1, setStep1] = useState(true);

  useEffect((): void => {
    !accounts.length && onAction();
  }, [accounts, onAction]);

  const _onCreate = useCallback((): void => {
    if (name && account) {
      setIsBusy(true);

      createAccountCesium(name, noPassword ? '' : password, account.seedHex, noPassword, 'ed25519', account.genesis)
        .then(() => onAction('/'))
        .catch((error): void => {
          setIsBusy(false);
          console.error(error);
        });
    }
  }, [account, name, password, noPassword, onAction]);

  const _onNextStep = useCallback(
    () => setStep1(false),
    []
  );

  const _onBackClick = useCallback(
    () => setStep1(true),
    []
  );

  const _onPasswordChange = useCallback((newPassword: string | null, isNoPassword: boolean) => {
    setPassword(newPassword || '');
    setNoPassword(isNoPassword);
  }, []);

  return (
    <>
      <HeaderWithSteps
        step={step1 ? 1 : 2}
        text={t('Import Cesium wallet')}
      />
      <div>
        <Address
          address={account?.address}
          genesisHash={account?.genesis}
          name={name}
        />
      </div>
      {step1
        ? (
          <CesiumIdPwd
            onAccountChange={setAccount}
            onNextStep={_onNextStep}
            type={'ed25519'}
          />
        )
        : (
          <AccountNamePasswordCreation
            buttonLabel={t('Add the account with this Cesium ID')}
            isBusy={isBusy}
            onBackClick={_onBackClick}
            onCreate={_onCreate}
            onNameChange={setName}
            onPasswordChange={_onPasswordChange}
          />
        )
      }
    </>
  );
}

export default ImportCesium;
