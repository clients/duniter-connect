// Copyright 2019-2025 @polkadot/extension-ui authors & contributors
// SPDX-License-Identifier: Apache-2.0

import type { FileType, GinkgoWallet, GinkgoWalletJSON, PubSecFile, ResponseJsonGetAccountInfo } from '@polkadot/extension-base/background/types';
import type { KeyringPair, KeyringPair$Json } from '@polkadot/keyring/types';
import type { KeyringPairs$Json } from '@polkadot/ui-keyring/types';

import React, { useCallback, useContext, useEffect, useRef, useState } from 'react';
import { u8aToHex, u8aToString } from '@polkadot/util';
import { AccountContext, ActionContext, Address, Button, InputFileWithLabel, InputWithLabel, Warning } from '../components/index.js';
import { useTranslation } from '../hooks/index.js';
import { batchRestore, jsonGetAccountInfo, jsonRestore } from '../messaging.js';
import { Header } from '../partials/index.js';
import { styled } from '../styled.js';
import { DEFAULT_TYPE } from '../util/defaultType.js';
import { isGinkgoWallet, isKeyringPairs$Json } from '../util/typeGuards.js';
import { encodeAddress } from '@polkadot/util-crypto/address';
import { base58Decode, base58Encode, ed25519PairFromSeed, mnemonicGenerate } from '@polkadot/util-crypto';
import keyring from '@polkadot/ui-keyring';
import { AccountsStore } from '@polkadot/extension-base/stores';
import PatternLock from 'react-pattern-lock';
import CryptoJS from 'crypto-js';

const acceptedFormats = ['application/json', 'text/plain'].join(', ');

interface Props {
  className?: string;
}

function Upload ({ className }: Props): React.ReactElement {
  const { t } = useTranslation();
  const { accounts } = useContext(AccountContext);
  const onAction = useContext(ActionContext);
  const [isBusy, setIsBusy] = useState(false);
  const [accountsInfo, setAccountsInfo] = useState<ResponseJsonGetAccountInfo[]>([]);
  const [password, setPassword] = useState<string>('');
  const [isFileError, setFileError] = useState(false);
  const [requirePassword, setRequirePassword] = useState(false);
  const [isPasswordError, setIsPasswordError] = useState(false);
  const [file, setFile] = useState<KeyringPair$Json | KeyringPairs$Json | GinkgoWallet | undefined>(undefined);
  const [showPatternLock, setShowPatternLock] = useState(false);
  const [patternError, setPatternError] = useState(false);
  const [lastPattern, setLastPattern] = useState<number[]>([]);

  useEffect((): void => {
    !accounts.length && onAction();
  }, [accounts, onAction]);

  const _onChangePass = useCallback(
    (pass: string): void => {
      setPassword(pass);
      setIsPasswordError(false);
    },
    []
  );

  const detectFileType = (content: string): FileType => {
    const firstLine = content.split('\n')[0].trim();
    if (firstLine === 'Type: PubSec') return 'PubSec';
    try {
      JSON.parse(content);
      return 'JSON';
    } catch {
      return 'Unknown';
    }
  };

  const parsePubSecFile = (content: string): PubSecFile | null => {
    const lines = content.split('\n');
    const result: Partial<Omit<PubSecFile, 'type'>> & { type: 'PubSec' } = { type: 'PubSec' };
    
    for (const line of lines) {
      const [key, value] = line.split(':').map(s => s.trim());
      if (key === 'Version' || key === 'pub' || key === 'sec') {
        (result as any)[key.toLowerCase()] = value;
      }
    }
  
    return (result.version && result.pub && result.sec) 
      ? result as PubSecFile 
      : null;
  };
  
  const _onChangeFile = useCallback(
    (file: Uint8Array): void => {
      setAccountsInfo(() => []);
      setFileError(false);
  
      const content = u8aToString(file);
      const fileType = detectFileType(content);
  
      switch (fileType) {
        case 'PubSec':
          const pubSecFile = parsePubSecFile(content);
          if (pubSecFile) {
            const jsonStandard = transformPubSecToJson(pubSecFile);
            setFile(jsonStandard);
            setRequirePassword(false);
            jsonGetAccountInfo(jsonStandard)
              .then((accountInfo) => setAccountsInfo((old) => [...old, accountInfo]))
              .catch((e) => {
                setFileError(true);
                console.error(e);
              });
          } else {
            setFileError(true);
          }
          break;
  
        case 'JSON':
          let json: KeyringPair$Json | KeyringPairs$Json | GinkgoWallet | undefined;
          try {
            json = JSON.parse(content) as KeyringPair$Json | KeyringPairs$Json | GinkgoWallet;
            setFile(json);
            
            if (isGinkgoWallet(json)) {
              setShowPatternLock(true);
              setRequirePassword(false);
            } else if (isKeyringPairs$Json(json)) {
              setRequirePassword(true);
              json.accounts.forEach((account) => {
                setAccountsInfo((old) => [...old, {
                  address: account.address,
                  genesisHash: account.meta.genesisHash,
                  name: account.meta.name
                } as ResponseJsonGetAccountInfo]);
              });
            } else {
              setRequirePassword(true);
              jsonGetAccountInfo(json)
                .then((accountInfo) => setAccountsInfo((old) => [...old, accountInfo]))
                .catch((e) => {
                  setFileError(true);
                  console.error(e);
                });
            }
          } catch (e) {
            console.error(e);
            setFileError(true);
          }
          break;
  
        case 'Unknown':
        default:
          setFileError(true);
          break;
      }
    },
    []
  );

  const _onRestore = useCallback(
    (): void => {
      if (!file) {
        return;
      }
  
      setIsBusy(true);
      const jsonFile = file as KeyringPair$Json | KeyringPairs$Json;
      (isKeyringPairs$Json(jsonFile) ? batchRestore(jsonFile, password ?? '') : jsonRestore(jsonFile, password ?? ''))
        .then(() => {
          onAction('/');
        })
        .catch((e) => {
          console.error(e);
          setIsPasswordError(true);
        })
        .finally(() => {
          setIsBusy(false);
        });
    },
    [file, onAction, password]
  );

  const timerRef = useRef<NodeJS.Timeout | null>(null);

  const _onPatternComplete = useCallback(
    (pattern: number[]): void => {
      setLastPattern([]);
      if (!file || !isGinkgoWallet(file) || pattern.length < 3) {
        return;
      }

      try {
        const ginkgoWalletJSON = decryptGinkgoKey(file.key, pattern);
        const jsonStandard = transformGinkgoWalletToKeyringPair(ginkgoWalletJSON);
        setFile(jsonStandard);
        jsonGetAccountInfo(jsonStandard)
          .then((accountInfo) => setAccountsInfo((old) => [...old, accountInfo]))
          .catch((e) => {
            setFileError(true);
            console.error(e);
          });

        if (timerRef.current) {
          clearTimeout(timerRef.current);
        }
        timerRef.current = setTimeout(() => {
          setShowPatternLock(false);
        }, 5);
      } catch (e) {
        console.error(e);
        setPatternError(true);
      }
    },
    [file]
  );

  const handlePatternChange = useCallback((pattern: number[]) => {
    console.log('Pattern changed:', pattern);
    setLastPattern(pattern);
    setPatternError(false);
  }, []);

  const handlePatternFinish = useCallback(() => {
    console.log('Pattern finished');
    _onPatternComplete(lastPattern);
  }, [_onPatternComplete, lastPattern]);

  return (
    <>
      <Header
        showBackArrow
        smallMargin
        text={t('Restore from file')}
      />
      <div className={className}>
        {accountsInfo.map(({ address, genesisHash, name, type = DEFAULT_TYPE }, index) => (
          <Address
            address={address}
            genesisHash={genesisHash}
            key={`${index}:${address}`}
            name={name}
            type={type}
          />
        ))}
        <InputFileWithLabel
          accept={acceptedFormats}
          isError={isFileError}
          label={t('backup file')}
          onChange={_onChangeFile}
          withLabel
        />
        {isFileError && (
          <Warning isDanger>
            {t('Invalid Json file')}
          </Warning>
        )}
        {showPatternLock && (
          <div className="pattern-lock-container">
            <PatternLock
              width={300}
              pointSize={15}
              size={3}
              path={lastPattern}
              onChange={handlePatternChange}
              onFinish={handlePatternFinish}
            />
          </div>
        )}
        {patternError && (
          <Warning isDanger>
            {t('Invalid pattern. Please try again.')}
          </Warning>
        )}
        {requirePassword && (
          <div>
            <InputWithLabel
              isError={isPasswordError}
              label={t('Password for this file')}
              onChange={_onChangePass}
              type='password'
            />
            {isPasswordError && (
              <Warning isBelowInput isDanger>
                {t('Unable to decode using the supplied passphrase')}
              </Warning>
            )}
          </div>
        )}
        <Button
          className='restoreButton'
          isBusy={isBusy}
          isDisabled={isFileError || isPasswordError || showPatternLock}
          onClick={_onRestore}
        >
          {t('Restore')}
        </Button>
      </div>
    </>
  );
}

function transformPubSecToJson(pubSecFile: PubSecFile): KeyringPair$Json {
  const { sec } = pubSecFile;
  
  const fullSecretKeyU8a = base58Decode(sec);
  const secretKeyU8a = fullSecretKeyU8a.slice(0, 32);
  const pair = ed25519PairFromSeed(secretKeyU8a);
  const address = encodeAddress(pair.publicKey);
  
  let keyringPair: KeyringPair;
  try {
    keyringPair = keyring.createFromUri(u8aToHex(secretKeyU8a), {}, 'ed25519');
  } catch (e) {
    keyring.loadAll({ store: new AccountsStore(), type: 'sr25519' });
    keyringPair = keyring.createFromUri(u8aToHex(secretKeyU8a), {}, 'ed25519');
  }

  const json = keyringPair.toJson();

  const name = mnemonicGenerate(12).split(' ')[0];
  
  return {
    ...json,
    address,
    meta: {
      ...json.meta,
      genesisHash: null,
      name: `PubSec Import ${name}`,
      whenCreated: Date.now()
    }
  };
}

function transformGinkgoWalletToKeyringPair(ginkgoWallet: GinkgoWalletJSON): KeyringPair$Json {
  const { seed, pubKey, name } = ginkgoWallet;
  const secretKeyU8a = Uint8Array.from(seed);
  const pair = ed25519PairFromSeed(secretKeyU8a);
  const address = encodeAddress(pair.publicKey);

  let keyringPair;
  try {
    keyringPair = keyring.createFromUri(u8aToHex(secretKeyU8a), {}, 'ed25519');
  } catch (e) {
    keyring.loadAll({ store: new AccountsStore(), type: 'sr25519' });
    keyringPair = keyring.createFromUri(u8aToHex(secretKeyU8a), {}, 'ed25519');
  }

  const json = keyringPair.toJson();

  const publicKeyBase58 = base58Encode(keyringPair.publicKey);
  if (publicKeyBase58 !== pubKey) {
    console.log('Duniter v1 pubkey:', publicKeyBase58);
    console.log('Duniter v1 target:', pubKey);
    throw new Error('Public keys do not match');
  }

  const finalName = name || mnemonicGenerate(12).split(' ')[0];

  return {
    ...json,
    address,
    meta: {
      ...json.meta,
      genesisHash: null,
      name: `Ginkgo Import ${finalName}`,
      whenCreated: Date.now()
    }
  };
}

function decryptGinkgoKey(encryptedKey: string, pattern: number[]): GinkgoWalletJSON {
  const paddedPassword = pattern.join('').padEnd(32, ' ');
  const iv = CryptoJS.lib.WordArray.create(new Array(16).fill(0));
  const ciphertext = CryptoJS.enc.Base64.parse(encryptedKey);

  const decryptedBytes = CryptoJS.AES.decrypt(
    CryptoJS.lib.CipherParams.create({ ciphertext }),
    CryptoJS.enc.Utf8.parse(paddedPassword),
    { iv, mode: CryptoJS.mode.CTR, padding: CryptoJS.pad.Pkcs7 }
  );

  const decryptedJson = JSON.parse(decryptedBytes.toString(CryptoJS.enc.Utf8));
  const cesiumCards = decryptedJson.cesiumCards ? JSON.parse(decryptedJson.cesiumCards) : [];
  const firstCard = cesiumCards.length > 0 ? cesiumCards[0] : null;
  const seedArray = JSON.parse(firstCard.seed);

  if (!Array.isArray(seedArray) || !seedArray.every(num => typeof num === 'number')) {
    throw new Error('Invalid seed format');
  }

  const ginkgoWallet: GinkgoWalletJSON = {
    pubKey: firstCard.pubKey,
    seed: seedArray,
    theme: firstCard.theme,
    name: firstCard.name
  };

  if (!ginkgoWallet.pubKey || !ginkgoWallet.seed || !ginkgoWallet.theme) {
    throw new Error('Decrypted data is missing required properties');
  }

  return ginkgoWallet;
}

export default styled(Upload)<Props>`
  .restoreButton {
    margin-top: 16px;
  }
  .pattern-lock-container {
    display: flex;
    justify-content: center;
    align-items: center;
  }
`;
