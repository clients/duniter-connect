// Copyright 2019-2024 @polkadot/extension-ui authors & contributors
// SPDX-License-Identifier: Apache-2.0

import React, { useCallback, useContext, useState } from 'react';
import { withRouter } from 'react-router';
import type { RouteComponentProps } from 'react-router';

import { ActionBar, ActionContext, ActionText, Button, Warning } from '../components/index.js';
import { useTranslation } from '../hooks/index.js';
import { forgetAccount } from '../messaging.js';
import { Header } from '../partials/index.js';
import { styled } from '../styled.js';
import { AccountContext } from '../components/contexts.js';

interface Props extends RouteComponentProps {
  className?: string;
}

function ForgetAll ({ className }: Props): React.ReactElement<Props> {
  const { t } = useTranslation();
  const onAction = useContext(ActionContext);
  const { accounts } = useContext(AccountContext);
  const [isBusy, setIsBusy] = useState(false);

  const _goHome = useCallback(
    () => onAction('/'),
    [onAction]
  );

  const _onClick = useCallback(
    async (): Promise<void> => {
      setIsBusy(true);
      
      try {
        // Supprimer tous les comptes un par un
        for (const account of accounts) {
          await forgetAccount(account.address);
        }
        setIsBusy(false);
        onAction('/');
      } catch (error) {
        setIsBusy(false);
        console.error(error);
      }
    },
    [accounts, onAction]
  );

  return (
    <>
      <Header
        showBackArrow
        text={t('Forget all accounts')}
      />
      <div className={className}>
        <Warning className='movedWarning'>
          {t('You are about to remove all accounts. This means that you will not be able to access them via this extension anymore. If you wish to recover them, you would need to use their seeds.')}
        </Warning>
        <div className='actionArea'>
          <Button
            isBusy={isBusy}
            isDanger
            onClick={_onClick}
          >
            {t('I want to forget all accounts')}
          </Button>
          <ActionBar className='withMarginTop'>
            <ActionText
              className='center'
              onClick={_goHome}
              text={t('Cancel')}
            />
          </ActionBar>
        </div>
      </div>
    </>
  );
}

export default styled(withRouter(ForgetAll))`
  .actionArea {
    padding: 10px 24px;
  }

  .center {
    margin: auto;
  }

  .movedWarning {
    margin-top: 8px;
  }

  .withMarginTop {
    margin-top: 4px;
  }
`; 