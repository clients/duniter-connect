// Copyright 2019-2024 @polkadot/extension-ui authors & contributors
// SPDX-License-Identifier: Apache-2.0

import React, { useCallback, useEffect, useState } from 'react';
import { Address, BackButton, ButtonArea, NextStepButton, Spinner, Warning } from '../../components/index.js';
import { useTranslation } from '../../hooks/index.js';
import { validateSeed, checkAccountsBalances } from '../../messaging.js';
import { styled } from '../../styled.js';
import Checkbox from '../../components/Checkbox.js';
import type { AccountInfo } from './index.js';
import type { KeypairType } from '@polkadot/util-crypto/types';

interface Props {
  className?: string;
  seed: string;
  type: KeypairType;
  onAccountsSelected: (accounts: AccountInfo[]) => void;
  onBackClick: () => void;
}

interface DerivationAccount extends AccountInfo {
  balance: string;
  derivationPath: string;
  selected: boolean;
}

// Maximum number of parallel requests
const BATCH_SIZE = 30;

function ScanAccounts ({ className, onAccountsSelected, onBackClick, seed, type }: Props): React.ReactElement {
  const { t } = useTranslation();
  const [accounts, setAccounts] = useState<DerivationAccount[]>([]);
  const [isScanning, setIsScanning] = useState(true);
  const [error, setError] = useState('');

  // Scan balances for a batch of derivation paths
  const scanBatch = useCallback(async (startIndex: number, endIndex: number) => {
    const batch = [];
    
    for (let i = startIndex; i <= endIndex; i++) {
      const derivationPath = i === 0 ? '' : `//${i}`;
      const suri = `${seed}${derivationPath}`;
      
      try {
        const account = await validateSeed(suri, type);
        batch.push({
          ...account,
          balance: '0',
          derivationPath,
          selected: true,
          suri
        });
      } catch (error) {
        console.error(`Error validating seed for path ${derivationPath}:`, error);
      }
    }

    return batch;
  }, [seed, type]);

  // Initial scan of all accounts
  useEffect(() => {
    const scanAllAccounts = async () => {
      try {
        const allAccounts: DerivationAccount[] = [];
        const totalAccounts = 30;
        
        // Process accounts in batches
        for (let i = 0; i <= totalAccounts; i += BATCH_SIZE) {
          const endIndex = Math.min(i + BATCH_SIZE - 1, totalAccounts);
          const batchAccounts = await scanBatch(i, endIndex);
          allAccounts.push(...batchAccounts);
        }

        // Check balances for all accounts
        const addresses = allAccounts.map(acc => acc.address);
        const balances = await checkAccountsBalances(addresses);
        
        // Update accounts with balances
        const accountsWithBalances = allAccounts.map(acc => ({
          ...acc,
          balance: balances.balances[acc.address] || '0'
        }));

        // Sort accounts by balance (descending) and keep only those with non-zero balance
        const sortedAccounts = accountsWithBalances
          .filter(acc => acc.balance !== '0')
          .sort((a, b) => {
            // If one of the accounts is the main account (without derivation), it should be first
            if (!a.derivationPath) return -1;
            if (!b.derivationPath) return 1;
            
            // For derived accounts, sort by derivation number
            const aNum = parseInt(a.derivationPath.replace('//', ''));
            const bNum = parseInt(b.derivationPath.replace('//', ''));
            return aNum - bNum;
          });

        if (sortedAccounts.length === 0) {
          // If no accounts are found, create the main account without derivation
          const defaultAccount: DerivationAccount = {
            address: '',
            balance: '0',
            derivationPath: '',
            selected: true,
            suri: seed
          };

          const validatedAccount = await validateSeed(defaultAccount.suri, type);
          defaultAccount.address = validatedAccount.address;
          onAccountsSelected([defaultAccount]);
          return;
        }

        setAccounts(sortedAccounts);
        setIsScanning(false);
      } catch (error) {
        console.error('Error scanning accounts:', error);
        setError(t('Une erreur est survenue lors du scan des comptes'));
        setIsScanning(false);
      }
    };

    void scanAllAccounts();
  }, [scanBatch, t]);

  const toggleAccount = useCallback((index: number) => {
    setAccounts((prevAccounts) => {
      const newAccounts = [...prevAccounts];
      newAccounts[index].selected = !newAccounts[index].selected;
      return newAccounts;
    });
  }, []);

  const toggleAll = useCallback(() => {
    setAccounts((prevAccounts) => {
      const someUnchecked = prevAccounts.some((acc) => !acc.selected);
      return prevAccounts.map((acc) => ({
        ...acc,
        selected: someUnchecked
      }));
    });
  }, []);

  const onSubmit = useCallback(() => {
    const selectedAccounts = accounts.filter((acc) => acc.selected);
    onAccountsSelected(selectedAccounts);
  }, [accounts, onAccountsSelected]);

  if (isScanning) {
    return (
      <div className={`${className} scanningContainer`}>
        <p>{t('Scan des comptes en cours...')}</p>
        <div className='spinnerWrapper'>
          <Spinner />
        </div>
      </div>
    );
  }

  if (error) {
    return (
      <Warning isDanger>
        {error}
      </Warning>
    );
  }

  const allSelected = accounts.every((acc) => acc.selected);
  const someSelected = accounts.some((acc) => acc.selected);

  return (
    <main className={className}>
      <p className='description'>
        {t('Veuillez choisir les comptes que vous souhaitez importer parmi ceux existant pour ce coffre :')}
      </p>
      <div className='selectAllRow'>
        <Checkbox
          checked={allSelected}
          indeterminate={!allSelected && someSelected}
          label={t('Sélectionner tout')}
          onChange={toggleAll}
        />
      </div>
      <div className='accounts'>
        {accounts.map((account, index) => (
          <label className='accountRow' key={account.address}>
            <Checkbox
              checked={account.selected}
              label=''
              onChange={() => toggleAccount(index)}
            />
            <Address
              address={account.address}
              name={account.derivationPath ? `${t('Compte')} ${account.derivationPath}` : t('Compte principal')}
            />
          </label>
        ))}
      </div>
      <div className='buttonWrapper'>
        <ButtonArea>
          <BackButton onClick={onBackClick} />
          <NextStepButton
            isDisabled={!accounts.some((acc) => acc.selected)}
            onClick={onSubmit}
          >
            {t('Importer les comptes sélectionnés')}
          </NextStepButton>
        </ButtonArea>
      </div>
    </main>
  );
}

export default styled(ScanAccounts)`
  display: flex;
  flex-direction: column;
  padding: 0 15px;
  overflow-y: auto;

  .description {
    margin: 8px 0 16px;
    text-align: center;
  }

  .selectAllRow {
    margin-bottom: 16px;
    padding: 0 8px;
  }

  .accounts {
    margin: 0 -15px;
    padding: 0 15px;
  }

  .accountRow {
    display: grid;
    grid-template-columns: auto 1fr;
    gap: 8px;
    align-items: center;
    padding: 8px;
    margin-bottom: 8px;
    border: 1px solid var(--border-color);
    border-radius: 4px;
    cursor: pointer;
  }

  .buttonWrapper {
    margin: 0 -15px;
    padding: 16px 15px;
    background: var(--background);
  }

  &.scanningContainer {
    justify-content: center;
    align-items: center;
    text-align: center;
    padding-top: 48px;

    .spinnerWrapper {
      position: relative;
      width: 50px;
      height: 50px;
      margin-top: 16px;
    }
  }
`; 