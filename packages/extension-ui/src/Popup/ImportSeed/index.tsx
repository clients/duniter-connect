// Copyright 2019-2025 @polkadot/extension-ui authors & contributors
// SPDX-License-Identifier: Apache-2.0

import React, { useCallback, useContext, useEffect, useState } from 'react';

import AccountNamePasswordCreation from '../../components/AccountNamePasswordCreation.js';
import { AccountContext, ActionContext } from '../../components/index.js';
import { useMetadata, useTranslation } from '../../hooks/index.js';
import { createAccountSuri } from '../../messaging.js';
import { HeaderWithSteps } from '../../partials/index.js';
import { DEFAULT_TYPE } from '../../util/defaultType.js';
import { LOCAL_STORAGE_SCAN_DERIVATION_KEY } from '@polkadot/extension-base/util/constants';
import SeedAndPath from './SeedAndPath.js';
import ScanAccounts from './ScanAccounts.js';
import type { HexString } from '@polkadot/util/types';

export interface AccountInfo {
  address: string;
  genesis?: HexString;
  suri: string;
}

function ImportSeed (): React.ReactElement {
  const { t } = useTranslation();
  const { accounts } = useContext(AccountContext);
  const onAction = useContext(ActionContext);
  const [isBusy, setIsBusy] = useState(false);
  const [account, setAccount] = useState<AccountInfo | null>(null);
  const [accountsToCreate, setAccountsToCreate] = useState<AccountInfo[]>([]);
  const [name, setName] = useState<string | null>(null);
  const [password, setPassword] = useState<string>('');
  const [noPassword, setNoPassword] = useState(false);
  const [step, setStep] = useState(1);
  const [type, setType] = useState(DEFAULT_TYPE);
  const [seed, setSeed] = useState('');
  const chain = useMetadata(account?.genesis, true);

  const scanDerivationEnabled = (() => {
    const saved = localStorage.getItem(LOCAL_STORAGE_SCAN_DERIVATION_KEY);
    return saved ? JSON.parse(saved) : true;
  })();

  useEffect((): void => {
    !accounts.length && onAction();
  }, [accounts, onAction]);

  useEffect((): void => {
    setType(
      chain && chain.definition.chainType === 'ethereum'
        ? 'ethereum'
        : DEFAULT_TYPE
    );
  }, [chain]);

  const _onCreate = useCallback(async (): Promise<void> => {
    if (!name || (!account && accountsToCreate.length === 0)) {
      return;
    }

    setIsBusy(true);

    try {
      if (accountsToCreate.length > 0) {
        // Create multiple accounts
        for (let i = 0; i < accountsToCreate.length; i++) {
          const acc = accountsToCreate[i];
          // Only add derivation number to name if we went through the scan step
          const derivationMatch = acc.suri.match(/\/\/(\d+)$/);
          const accName = accountsToCreate.length === 1 && acc.suri.includes('/')
            ? name // Single account with derivation path, use name as is
            : derivationMatch?.[1] // Multiple accounts from scan, add derivation number
              ? `${name} ${derivationMatch[1]}`
              : name;

          await createAccountSuri(accName, noPassword ? '' : password, acc.suri, type, acc.genesis);
        }
      } else if (account) {
        // Create single account
        await createAccountSuri(name, noPassword ? '' : password, account.suri, type, account.genesis);
      }

      onAction('/');
    } catch (error) {
      setIsBusy(false);
      console.error(error);
    }
  }, [account, accountsToCreate, name, password, noPassword, onAction, type]);

  const _onAccountChange = useCallback((newAccount: AccountInfo | null): void => {
    setAccount(newAccount);
    if (newAccount?.suri) {
      setSeed(newAccount.suri);
    }
  }, []);

  const _onNextStep = useCallback(() => {
    if (step === 1 && account?.suri) {
      if (account.suri.includes('/') || !scanDerivationEnabled) {
        setAccountsToCreate([account]);
        setStep(3);
      } else {
        setStep(2);
      }
    } else {
      setStep(step + 1);
    }
  }, [step, account, scanDerivationEnabled]);

  const _onBackClick = useCallback(() => {
    setStep(step - 1);
  }, [step]);

  const _onPasswordChange = useCallback((newPassword: string | null, isNoPassword: boolean) => {
    setPassword(newPassword || '');
    setNoPassword(isNoPassword);
  }, []);

  const _onAccountsSelected = useCallback((selectedAccounts: AccountInfo[]) => {
    setAccountsToCreate(selectedAccounts);
    setStep(3);
  }, []);

  return (
    <>
      <HeaderWithSteps
        step={step}
        text={t('Import account')}
        totalSteps={scanDerivationEnabled ? 3 : 2}
      />
      {step === 1 && (
        <SeedAndPath
          onAccountChange={_onAccountChange}
          onNextStep={_onNextStep}
          type={type}
        />
      )}
      {step === 2 && seed && (
        <ScanAccounts
          onAccountsSelected={_onAccountsSelected}
          onBackClick={_onBackClick}
          seed={seed}
          type={type}
        />
      )}
      {step === 3 && (
        <AccountNamePasswordCreation
          buttonLabel={t('Add the account with the supplied seed')}
          isBusy={isBusy}
          onBackClick={_onBackClick}
          onCreate={_onCreate}
          onNameChange={setName}
          onPasswordChange={_onPasswordChange}
        />
      )}
    </>
  );
}

export default ImportSeed;
