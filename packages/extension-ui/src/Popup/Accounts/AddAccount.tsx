// Copyright 2019-2025 @polkadot/extension-ui authors & contributors
// SPDX-License-Identifier: Apache-2.0

import React from 'react';
import { MenuDivider } from '../../components/index.js';
import { useTranslation } from '../../hooks/index.js';
import Header from '../../partials/Header.js';
import { styled } from '../../styled.js';
import AccountMenuList from './AccountMenuList';

interface Props {
  className?: string;
}

function AddAccount ({ className }: Props): React.ReactElement<Props> {
  const { t } = useTranslation();

  return (
    <>
      <Header
        showAdd
        showSettings
        text={t('Add Account')}
      />
      <div className={className}>
        <div className='intro'>
          <p>{t("You currently don't have any accounts. Create your first account to get started.")}</p>
        </div>
        <MenuDivider />
        <AccountMenuList />
      </div>
    </>
  );
}

export default React.memo(styled(AddAccount)<Props>`
  color: var(--textColor);
  height: 100%;
  display: flex;
  flex-direction: column;
  padding: 20px;

  .intro {
    text-align: center;
    margin-bottom: 20px;
  }

  .intro p {
    font-size: 18px;
    line-height: 1.5;
    color: var(--textColor);
    opacity: 0.8;
  }
`);
