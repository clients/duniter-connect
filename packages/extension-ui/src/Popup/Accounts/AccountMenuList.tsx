// Copyright 2019-2024 @polkadot/extension-ui authors & contributors
// SPDX-License-Identifier: Apache-2.0

import React, { useCallback, useContext } from 'react';
import { Link, MenuItem, AccountContext } from '../../components/index.js';
import { useTranslation } from '../../hooks/index.js';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFileUpload, faKey, faPlusCircle, faTrash, type IconDefinition } from '@fortawesome/free-solid-svg-icons';
import useIsPopup from '@polkadot/extension-ui/hooks/useIsPopup';
import cs from '../../assets/cesium_logo.svg';
import { styled } from '../../styled.js';
import { windowOpen } from '@polkadot/extension-ui/messaging';

interface Props {
    className?: string;
}

interface IconProps {
  icon: IconDefinition | string;
  isSvg?: boolean;
}

const Icon: React.FC<IconProps> = ({ icon, isSvg = false }) => {
  if (isSvg) {
    return <img className='logo' src={icon as string} alt="Icon" />;
  }
  return <FontAwesomeIcon icon={icon as IconDefinition} />;
};

function AccountMenuList ({ className }: Props): React.ReactElement<Props> {
  const { t } = useTranslation();
  const isPopup = useIsPopup();
  const jsonPath = '/account/restore-json';
  const { accounts } = useContext(AccountContext);

  const _openJson = useCallback(
    (): void => {
      windowOpen(jsonPath).catch(console.error);
    }, []
  );

  return (
      <div className={className}>
        <div className='menu-container'>
        <MenuItem className='menuItem'>
            <Link to='/account/import-seed'>
            <Icon icon={faKey} />
            <span>{t('Import mnemonic')}</span>
            </Link>
        </MenuItem>
        <MenuItem className='menuItem'>
            <Link to='/account/import-cesium'>
            <Icon icon={cs} isSvg={true} />
            <span>{t('Import Cesium wallet')}</span>
            </Link>
        </MenuItem>
        <MenuItem className='menuItem'>
            <Link
            onClick={isPopup ? _openJson : undefined}
            to={isPopup ? undefined : jsonPath}
            >
            <Icon icon={faFileUpload} />
            <span>{t('Restore account from backup file')}</span>
            </Link>
        </MenuItem>
        <MenuItem className='menuItem'>
            <Link to={'/account/create'}>
            <Icon icon={faPlusCircle} />
            <span>{t('Create new account')}</span>
            </Link>
        </MenuItem>
        {accounts.length > 0 && (
          <MenuItem className='menuItem'>
            <Link 
              isDanger
              to={'/account/forget-all'}
            >
              <Icon icon={faTrash} />
              <span>{t('Forget all accounts')}</span>
            </Link>
          </MenuItem>
        )}
        </div>
    </div>
  );
};

export default React.memo(styled(AccountMenuList)<Props>`
  .menu-container {
    display: flex;
    flex-direction: column;
    gap: 15px;
  }

  .menuItem {
    background-color: rgba(0, 100, 255, 0.1);
    border-radius: 8px;
    transition: background-color 0.2s;

    &:hover {
      background-color: rgba(0, 100, 255, 0.3);
    }
  }

  .menuItem a {
    display: flex;
    align-items: center;
    padding: 15px;
    text-decoration: none;
    color: var(--textColor);
  }

  .logo,
  .menuItem svg {
    width: 24px;
    height: 24px;
    margin-right: 15px;
    color: var(--iconNeutralColor)
  }

  .menuItem span {
    font-size: 16px;
  }
`);
