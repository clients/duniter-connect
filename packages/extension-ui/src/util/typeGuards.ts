// Copyright 2019-2025 @polkadot/extension-ui authors & contributors
// SPDX-License-Identifier: Apache-2.0

import type { GinkgoWallet } from '@polkadot/extension-base/background/types';
import type { KeyringPair$Json } from '@polkadot/keyring/types';
import type { KeyringPairs$Json } from '@polkadot/ui-keyring/types';

export function isKeyringPairs$Json (json: KeyringPair$Json | KeyringPairs$Json): json is KeyringPairs$Json {
  return (json.encoding.content).includes('batch-pkcs8');
}

export function isGinkgoWallet(json: KeyringPair$Json | KeyringPairs$Json | GinkgoWallet): json is GinkgoWallet {
  return json && typeof json === 'object' && 'key' in json && typeof json.key === 'string';
}
