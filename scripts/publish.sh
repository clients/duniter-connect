#!/bin/bash

set -euo pipefail

# Chargement des variables d'environnement
if [[ ! -f .env ]]; then
    echo "Erreur: Fichier .env non trouvé" >&2
    exit 1
fi

# Charger les variables d'environnement sans les exporter
while IFS='=' read -r key value
do
    if [[ ! $key =~ ^# && -n $key ]]; then
        eval "$key='$value'"
    fi
done < .env

# Fonctions utilitaires
log() {
    echo "[INFO] $1"
}

exit_script() {
    echo "[ERREUR] $1" >&2
    exit 1
}

# Vérification des variables requises
required_vars=(
    "CHROME_CLIENT_ID" "CHROME_CLIENT_SECRET" "CHROME_REFRESH_TOKEN" "CHROME_EXTENSION_ID"
    "FIREFOX_JWT_ISSUER" "FIREFOX_JWT_SECRET" "FIREFOX_ADDON_ID"
)

for var in "${required_vars[@]}"; do
    if [[ -z "${!var-}" ]]; then
        exit_script "La variable $var n'est pas définie dans le fichier .env"
    fi
done

# Configuration des fichiers de build
CHROME_BUILD="master-chrome-build.zip"
FIREFOX_BUILD="master-ff-build.zip"
FIREFOX_SRC="master-ff-src.zip"

# Fonction pour mettre à jour l'extension Chrome
## access token

check_and_renew_refresh_token() {
    log "Vérification du refresh token..."
    local token_response
    token_response=$(curl -s -d "client_id=$CHROME_CLIENT_ID&client_secret=$CHROME_CLIENT_SECRET&refresh_token=$CHROME_REFRESH_TOKEN&grant_type=refresh_token" https://accounts.google.com/o/oauth2/token)
    
    if [[ $(echo "$token_response" | jq -r .error) == "invalid_grant" ]]; then
        log "Le refresh token est invalide ou expiré. Obtention d'un nouveau token..."
        get_new_refresh_token
    else
        log "Le refresh token est valide."
    fi
}

get_new_refresh_token() {
    log "Pour obtenir un nouveau refresh token, veuillez suivre ces étapes :"
    log "1. Ouvrez ce lien dans votre navigateur :"
    log "https://accounts.google.com/o/oauth2/auth?client_id=$CHROME_CLIENT_ID&response_type=code&redirect_uri=urn:ietf:wg:oauth:2.0:oob&scope=https://www.googleapis.com/auth/chromewebstore&access_type=offline&prompt=consent"
    log "2. Autorisez l'accès et copiez le code fourni."
    log "3. Entrez le code ici :"
    
    read -p "Code : " auth_code
    
    local new_token_response
    new_token_response=$(curl -s -d "client_id=$CHROME_CLIENT_ID&client_secret=$CHROME_CLIENT_SECRET&code=$auth_code&grant_type=authorization_code&redirect_uri=urn:ietf:wg:oauth:2.0:oob" https://oauth2.googleapis.com/token)
    
    local new_refresh_token
    new_refresh_token=$(echo "$new_token_response" | jq -r .refresh_token)
    
    if [[ -z "$new_refresh_token" || "$new_refresh_token" == "null" ]]; then
        exit_script "Impossible d'obtenir un nouveau refresh token. Réponse : $new_token_response"
    fi
    
    # Mise à jour du fichier .env avec le nouveau refresh token
    awk -v rt="$new_refresh_token" '/^CHROME_REFRESH_TOKEN=/{$0="CHROME_REFRESH_TOKEN="rt} 1' .env > .env.tmp && mv .env.tmp .env
    
    CHROME_REFRESH_TOKEN=$new_refresh_token
    log "Nouveau refresh token obtenu et enregistré dans le fichier .env"
}

update_chrome() {
    log "Mise à jour de l'extension Chrome..."

    # Création du fichier de build
    yarn build:chrome

    # Vérification et renouvellement du refresh token si nécessaire
    check_and_renew_refresh_token

    # Obtention du jeton d'accès
    local access_token
    local access_token_response
    access_token_response=$(curl -s -d "client_id=$CHROME_CLIENT_ID&client_secret=$CHROME_CLIENT_SECRET&refresh_token=$CHROME_REFRESH_TOKEN&grant_type=refresh_token" https://accounts.google.com/o/oauth2/token)
    access_token=$(echo "$access_token_response" | jq -r .access_token)

    if [[ -z "$access_token" || "$access_token" == null ]]; then
        exit_script "Impossible d'obtenir le jeton d'accès pour Chrome: access_token_response: $access_token_response"
    fi

    # Mise à jour de l'extension
    local response
    response=$(curl -H "Authorization: Bearer $access_token" \
         -H "x-goog-api-version: 2" \
         -X PUT \
         -F "file=@$CHROME_BUILD" \
         https://www.googleapis.com/upload/chromewebstore/v1.1/items/$CHROME_EXTENSION_ID)

    if [[ $(echo "$response" | jq -r .uploadState) != "SUCCESS" ]]; then
        exit_script "Échec de la mise à jour de l'extension Chrome: $response"
    fi

    log "Extension Chrome mise à jour avec succès."
    publish_chrome "$access_token"
}

publish_chrome() {
    local access_token

    if [[ -z "$1" ]]; then
        log "Obtention du jeton d'accès pour Chrome..."
        access_token=$(curl -s -d "client_id=$CHROME_CLIENT_ID&client_secret=$CHROME_CLIENT_SECRET&refresh_token=$CHROME_REFRESH_TOKEN&grant_type=refresh_token" https://accounts.google.com/o/oauth2/token | jq -r .access_token)

        if [[ -z "$access_token" ]]; then
            exit_script "Impossible d'obtenir le jeton d'accès pour Chrome"
        fi
    else
        access_token="$1"
    fi

    log "Soumission de l'extension pour révision..."
    local publish_response
    publish_response=$(curl -s -w "%{http_code}" -H "Authorization: Bearer $access_token" \
        -H "x-goog-api-version: 2" \
        -H "Content-Length: 0" \
        -X POST \
        https://www.googleapis.com/chromewebstore/v1.1/items/$CHROME_EXTENSION_ID/publish)

    http_status=${publish_response: -3}
    response_body=${publish_response:0:${#publish_response}-3}

    if [[ "$http_status" == "200" ]]; then
        log "Extension soumise pour révision avec succès"
    else
        exit_script "Échec de la soumission pour révision. Code HTTP: $http_status, Réponse: $response_body"
    fi
}

generate_jwt() {
    local now=$(date +%s)
    local exp=$((now + 300))
    
    local header=$(echo -n '{"alg":"HS256","typ":"JWT"}' | openssl base64 -e -A | tr '+/' '-_' | tr -d '=')
    local payload=$(echo -n "{\"iss\":\"$FIREFOX_JWT_ISSUER\",\"iat\":$now,\"exp\":$exp}" | openssl base64 -e -A | tr '+/' '-_' | tr -d '=')
    local signature=$(echo -n "$header.$payload" | openssl dgst -binary -sha256 -hmac "$FIREFOX_JWT_SECRET" | openssl base64 -e -A | tr '+/' '-_' | tr -d '=')
    
    echo "$header.$payload.$signature"
}

# Fonction pour mettre à jour l'extension Firefox
update_firefox() {
    log "Mise à jour de l'extension Firefox..."

    # Création du fichier de build
    yarn build:ff

    JWT_TOKEN=$(generate_jwt)

    # Téléchargement du fichier pour validation
    local upload_response
    upload_response=$(curl -X POST "https://addons.mozilla.org/api/v5/addons/upload/" \
         -H "Authorization: JWT $JWT_TOKEN" \
         -F "upload=@$FIREFOX_BUILD" \
         -F "channel=listed")

    local upload_uuid
    upload_uuid=$(echo "$upload_response" | jq -r .uuid)

    if [[ -z "$upload_uuid" ]]; then
        exit_script "Échec du téléchargement de l'extension Firefox: $upload_response"
    fi

    # Vérification du statut de la validation
    local max_attempts=30
    local attempt=0
    local validation_status

    while [[ $attempt -lt $max_attempts ]]; do
        validation_response=$(curl -s "https://addons.mozilla.org/api/v5/addons/upload/$upload_uuid/" \
            -H "Authorization: JWT $JWT_TOKEN")
        
        processed=$(echo $validation_response | jq -r '.processed')
        
        if [[ "$processed" == "true" ]]; then
            log "Traitement terminé. Vérification du statut final..."
            break
        fi

        log "En attente du traitement... (tentative $((attempt + 1))/$max_attempts)"
        log "Réponse actuelle : $validation_response"
        sleep 10
        attempt=$((attempt + 1))
    done

    if [[ $attempt -eq $max_attempts ]]; then
        exit_script "Délai d'attente dépassé pour la validation de l'extension Firefox"
    fi

    # Création d'une nouvelle version
    local version_response
    version_response=$(curl -X POST "https://addons.mozilla.org/api/v5/addons/addon/$FIREFOX_ADDON_ID/versions/" \
         -H "Authorization: JWT $JWT_TOKEN" \
         -H "Content-Type: application/json" \
         -d "{\"upload\": \"$upload_uuid\"}")

    if [[ $(echo "$version_response" | jq -r .file.status) != "unreviewed" ]]; then
        exit_script "Échec de la création de la nouvelle version Firefox: $version_response"
    fi

    # # Téléchargement du code source
    # VERSION_ID=$(echo "$version_response" | jq -r .id)
    # echo "Version ID: $VERSION_ID"
    # local src_response=$(curl -s -XPATCH "https://addons.mozilla.org/api/v5/addons/addon/duniter-connect/versions/$VERSION_ID/" \
    #   -H "Authorization: JWT $JWT_TOKEN" \
    #   -F "source=@source.zip" \
    #   -F "license=Apache-2.0")

    # if [[ $(echo "$src_response" | jq -r .processed) != "true" ]]; then
    #     exit_script "Échec du téléchargement du code source Firefox: $src_response"
    # fi

    log "Extension Firefox mise à jour avec succès"
}

# Vérification des arguments
if [[ $# -eq 0 ]]; then
    exit_script "Usage: $0 [chrome|firefox|all] [publish]"
fi

# Exécution de la mise à jour en fonction de l'argument
case "$1" in
    chrome)
        if [[ $# -eq 2 && "$2" == "publish" ]]; then
            publish_chrome ""
        else
            update_chrome
        fi
        ;;
    firefox|ff)
        update_firefox
        ;;
    all)
        update_firefox
        update_chrome
        ;;
    *)
        exit_script "Argument invalide. Utilisez 'chrome', 'firefox' ou 'all'."
        ;;
esac

log "Mise à jour terminée avec succès"

# Affichage des liens de statut
case "$1" in
    chrome|all)
        log "Statut de version Chrome : https://chrome.google.com/webstore/devconsole/f0d1a02c-e42f-45a7-90a2-434ffe245c3a/$CHROME_EXTENSION_ID/edit/package"
        ;;
esac

case "$1" in
    firefox|ff|all)
        log "Statut de version Firefox : https://addons.mozilla.org/fr/developers/addon/duniter-connect/versions"
        ;;
esac
